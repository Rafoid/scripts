--Oracle CRSCTL Commands List
--CRACTL utility allows you to administer cluster resources. Here are few quick commands to help you administer Oracle RAC cluster!
=========================================================================================================================================
--CHECK CLUSTER STATUS

--Status of upper & lower stack
./crsctl check crs

--Status of upper stack
./crsctl check cluster

--Cluster status on all nodes
./crsctl check cluster -all

--Cluster status on specific node
./crsctl check cluster -n rac2

=========================================================================================================================================
--CHECK CLUSTER NODES
--Check cluster services in table format
./crsctl status resource -t

--Checking status of clusterware nodes / services
./crsctl status server -f

--Check cluster nodes
olsnodes -n
oraracn1        1
oraracn2        2

=========================================================================================================================================
--STOP/START GRID CLUSTER

--Stop/Start HAS on current node
./crsctl stop has
./crsctl start has 

--Stop/Start HAS on remote node
./crsctl stop has –n rac2
./crsctl start has –n rac2

--Stop/Start entire cluster on all nodes
./crsctl stop cluster -all
./crsctl start cluster –all

--Stop/Start cluster ( CRS + HAS ) on remote node
./crsctl stop cluster –n rac2 
./crsctl start cluster –n rac2

=========================================================================================================================================
--ENABLE/DISABLE CLUSTER AUTO START
crsctl disable has
crsctl enable has